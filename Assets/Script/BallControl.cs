﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using SWNetwork;

public class BallControl : MonoBehaviour
{
    NetworkID networkID;
    public int scoreP1;
    public int scoreP2;
    public int force;
    GameObject scoreP1UI;
    GameObject scoreP2UI;
    public GameObject panel;
    RemoteEventAgent remoteEventAgent;

    // Start is called before the first frame update
    void Start()
    {
        networkID = GetComponent<NetworkID>();
        panel.gameObject.SetActive(false);
        remoteEventAgent = GetComponent<RemoteEventAgent>();

        GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 2).normalized * force);
        scoreP1 = 0;
        scoreP2 = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnCollisionEnter2D(Collision2D other) {
        if (other.gameObject.tag == "batas_bawah")
        {
            //ResetBall();
            //Invoke("ResetBall", 0f);
            StartCoroutine(ResetBallEnum(0f, 1));
            BallScore(1);
            //GetComponent<Rigidbody2D>().AddForce(new Vector2(1, -2).normalized * force);
        }

        else if(other.gameObject.tag == "batas_atas")
        {
            //ResetBall();
            //Invoke("ResetBall", 0f);
            StartCoroutine(ResetBallEnum(0f, 1));
            BallScore(2);
            //GetComponent<Rigidbody2D>().AddForce(new Vector2(1, 2).normalized * force);
        }

        if (other.gameObject.tag == "Player")
        {
            Vector2 dir = new Vector2(GetComponent<Rigidbody2D>().velocity.x, GetComponent<Rigidbody2D>().velocity.y).normalized;
            GetComponent<Rigidbody2D>().AddForce(dir * force);
        }
        //if (networkID.IsMine)
        //{
        //    if(other.gameObject.tag == "Player")
        //    {
        //        Vector2 dir = new Vector2(GetComponent<Rigidbody2D>().velocity.x, GetComponent<Rigidbody2D>().velocity.y).normalized;
        //        GetComponent<Rigidbody2D>().AddForce(dir * force);
        //    }
        //}
    }

    IEnumerator ResetBallEnum(float delayTime, int val) {
        yield return new WaitForSeconds(delayTime);

        SWNetworkMessage msg = new SWNetworkMessage();
        Vector3 ResetPosition = new Vector3(0, 0, val);
        msg.Push(ResetPosition);
        remoteEventAgent.Invoke("ResetPosition", msg);
    }

    void ResetBall()
    {
       SWNetworkMessage msg = new SWNetworkMessage();
       Vector3 ResetPosition = new Vector3(0, 0, 0);
       msg.Push(ResetPosition);
       remoteEventAgent.Invoke("ResetPosition", msg);
    }

    public void RemoteResetBall(SWNetworkMessage msg)
    {
        Vector3 pos = msg.PopVector3();
        Vector2 newPos = new Vector2(pos.x, pos.y);
        this.transform.position = pos;
        GetComponent<Rigidbody2D>().AddForce(new Vector2(1, 2 * pos.z).normalized * force);
    }

    void BallScore(int teams)
    {
        GameMaanager gameMaanager = FindObjectOfType<GameMaanager>();
        gameMaanager.AddScoreTeam(teams);
    }

    public void Exit()
    {
        NetworkClient.Instance.DisconnectFromRoom();
        NetworkClient.Lobby.LeaveRoom(HandleLeaveRoom);
    }

    void HandleLeaveRoom(bool okay, SWLobbyError error)
    {
        if (!okay)
        {
            Debug.LogError(error);
        }

        Debug.Log("Left room");
         UnityEngine.SceneManagement.SceneManager.LoadScene("SWNetwork/Scenes/LobbyScene");
    }
}

