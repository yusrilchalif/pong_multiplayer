using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SWNetwork;
using System;

public class GameMaanager : MonoBehaviour
{
    private int scoreP1, scoreP2;
    NetworkID networkID;
    RoomPropertyAgent roomPropertyAgent;

    [SerializeField]
    private GameObject Ball;
    [SerializeField]
    private GameObject Player;
    [SerializeField]
    private Text ScoreUpdate;
    [SerializeField]
    private GameObject GameOverPanel;
    [SerializeField]
    private Text scoretext;

    [Serializable]
    public class Score
    {
        public int scoreP1;
        public int scoreP2;
    }


    // Start is called before the first frame update
    void Start()
    {
        networkID = GetComponent<NetworkID>();
        roomPropertyAgent = GetComponent<RoomPropertyAgent>();

        scoreP1 = 0;
        scoreP2 = 0;
    }

   public void AddScoreTeam(int team)
   {
       ScoreNetworkUpdate(team);
       Debug.Log("Player goals = " + team);
   }

   public void UpdateScore(int val1, int val2)
   {
       ScoreUpdate.text = val1.ToString() + " - " + val2.ToString();
   }

   public void ScoreNetworkUpdate(int team)
   {
        SWSyncedProperty agent = roomPropertyAgent.GetPropertyWithName("gameScores");

        Score GameScore = null;

        if (agent != null)
        {
            GameScore = agent.GetValue<Score>();
        }

        if(GameScore == null)
        {
            GameScore = new Score();
        }

        bool foundGameScore = false;

        if(team == 1)
        {
            GameScore.scoreP1++;
            foundGameScore = true;
        } 
        else
        {
            GameScore.scoreP2++;
            foundGameScore = true;
        }
        ScoreUpdate.text = GameScore.scoreP1.ToString() + " - " + GameScore.scoreP2.ToString();
        roomPropertyAgent.Modify<Score>("gameScores", GameScore);
   }

   public void OnScoreChange()
   {
       Score GameScore = roomPropertyAgent.GetPropertyWithName("gameScores").GetValue<Score>();
       Debug.Log(GameScore);

       if(GameScore != null)
       {
           if(GameScore.scoreP1 == 5 || GameScore.scoreP2 == 5)
           {
            //    networkID.Destroy(2.5f);
               GameOverPanel.SetActive(true);
               Ball.SetActive(false);
               Player.SetActive(false);
               scoretext.text = GameScore.scoreP1 + " - " + GameScore.scoreP2;
           }
       }
   }
}
