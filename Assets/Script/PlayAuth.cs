﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.Multiplayer;

public class PlayAuth : MonoBehaviour
{
    public static PlayGamesPlatform platform;
    public string leaderboard;
    public Button login;
    public Button logout;
    public Button play;
    public Button achievement;
    // public Button leaderboard;

    void Start()
    {
        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
                                                .Build();
        PlayGamesPlatform.DebugLogEnabled = true;
        PlayGamesPlatform.Activate();
        logout.gameObject.SetActive(false);
        login.gameObject.SetActive(true);
        play.gameObject.SetActive(false);
        achievement.gameObject.SetActive(false);
        // leaderboard.gameObject.SetActive(false);
    }

    public void SignIn()
    {
        Social.localUser.Authenticate((bool success) =>
        {
            if(success)
            {
                Debug.Log("Success");
                LoginAchievement();
                logout.gameObject.SetActive(true);
                play.gameObject.SetActive(true);
                login.gameObject.SetActive(false);
                achievement.gameObject.SetActive(true);
            }
            else
            {
                Debug.Log("Failled");
                logout.gameObject.SetActive(true);
                play.gameObject.SetActive(true);
                login.gameObject.SetActive(false);
                achievement.gameObject.SetActive(true);
            }
        });

    }

    public void Logout()
    {
        PlayGamesPlatform.Instance.SignOut();
        logout.gameObject.SetActive(false);
        login.gameObject.SetActive(true);
        play.gameObject.SetActive(false);
    }

    public void Play(){
         UnityEngine.SceneManagement.SceneManager.LoadScene("SWNetwork/Scenes/LobbyScene");
    }

    public void Leaderboard()
    {
        PlayGamesPlatform.Instance.ShowLeaderboardUI(leaderboard);
    }

    public void ShowAchievements() {
        if (PlayGamesPlatform.Instance.localUser.authenticated) {
            PlayGamesPlatform.Instance.ShowAchievementsUI();
        }
        else {
          Debug.Log("Cannot show Achievements, not logged in");
        }
    }

    public void LoginAchievement()
    {
        Social.ReportProgress(GPGSIds.achievement_welcome, 100f, null);
    }
}