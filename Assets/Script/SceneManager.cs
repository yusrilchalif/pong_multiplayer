﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SWNetwork;
using UnityEngine.SceneManagement;

public class SceneManager : MonoBehaviour
{
    // public GameObject panel;
    

    void Start(){
        
    }

   public void OnSpawnerReady(bool alreadySetup, SceneSpawner sceneSpawener)
   {
       if(NetworkClient.Instance.IsHost)
       {
           sceneSpawener.SpawnForPlayer(0,1);
       }
       else
       {
           sceneSpawener.SpawnForPlayer(0, 0);
       }

       sceneSpawener.PlayerFinishedSceneSetup();
   }

   public void Exit()
   {
       NetworkClient.Instance.DisconnectFromRoom();
       NetworkClient.Lobby.LeaveRoom(HandleLeaveRoom);
   }

   void HandleLeaveRoom(bool okay, SWLobbyError error)
   {
       if(!okay)
       {
           Debug.Log(error);
       }
       Debug.Log("Exit room");
        UnityEngine.SceneManagement.SceneManager.LoadScene("SWNetwork/Scenes/LobbyScene");
   }
}